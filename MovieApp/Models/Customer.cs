﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApp.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="This field is required")]
        [MaxLength(255)]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime? Birthday { get; set; }

        //public MembershipType TypeOfMembership { get; set; }

        /*[Display(Name = "Membership Type")]
        public int MembershipTypeId { get; set; }*/
    }
}