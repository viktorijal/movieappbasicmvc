﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Dtos
{
    public class NewRentalDto
    {
        public int CustomerId { get; set; }
        public IList<int> MovieIds { get; set; }
    }
}