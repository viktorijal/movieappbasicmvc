﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieApp.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime DateAdded { get; set; }

        [Range(1, 20, ErrorMessage = "The number in stock should be between 1 and 20")]
        public int NumberInStock { get; set; }
        public GenreDto Genre { get; set; }
        public int GenreId { get; set; }
    }
}